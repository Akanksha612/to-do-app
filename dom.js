let count = 0;
let action = document.getElementById("checkbox");
let mylist = document.getElementById("mylist");
let bottom_box = document.getElementById("bottom-box");
let enter_key = document.getElementById("enterme");
enter_key.addEventListener("keyup", additems);

function additems(e) {
  if (e.keyCode === 13) {
    //enterkey
    let addme = document.getElementById("enterme").value; //Important - receive value here only, not outside the function.

    if (addme === "") {
      alert("Task not given");
      return;
    }

    //create a new li item
    let li = document.createElement("li");

    //li will have 2 children - checkbox & Textcontent

    let lab = document.createElement("INPUT");
    lab.setAttribute("type", "checkbox");
    lab.setAttribute("class", "checkbox");

    li.appendChild(lab);

    li.appendChild(document.createTextNode(addme));
    mylist.appendChild(li);

    let btn = document.createElement("button");
    btn.textContent = "X";
    btn.setAttribute("id", "mybutton");
    li.appendChild(btn);

    count++;

    update_counter();

    mylist.addEventListener("click", responding);

    document.getElementById("enterme").value = "";
  }
}

//Event delegation

function responding(e) {
  console.log(e.target);

  if (e.target.id === "mybutton") {
    delete_item(e);
  }

  if (e.target.classList.contains("checkbox")) {
    change_text_style(e);
  }
}

function change_text_style(e) {
  if (e.target.checked) {
    e.target.parentNode.classList.add("checked-cut");

    count--;

    if (count < 0) counter.textContent = 0 + " items left";
    else {
      update_counter();
    }
  } //checkbox unclicked again
  else {
    e.target.parentNode.classList.remove("checked-cut");

    count++;

    update_counter();
  }
}

function delete_item(e) {
  e.target.parentNode.remove();

  count--;

  if (count < 0) counter.textContent = 0 + " items left";
  else {
    update_counter();
  }
}

//Event delegation

bottom_box.addEventListener("click", states);

function states(e) {
  if (e.target.id == "button-1") show_all(e);

  if (e.target.id == "button-2") show_incomplete(e);

  if (e.target.id == "button-3") show_complete(e);

  if (e.target.id == "button-4") clear_complete(e);
}

function show_all(e) {
  let obj = document.getElementById("mylist");

  //obj(object)= {id="mylist", children:{0: li(object), 1: li(object), 2: li(object), 3: li(object)}, lastElementChild: li(object)}

  let object = obj.children;

  Array.from(object).forEach((item) => {
    item.style.display = "block";
  });
}

function show_incomplete(e) {
  //traverse ul
  //if unchecked - show
  //else - dont show

  let obj = document.getElementById("mylist");

  let object = obj.children;

  Array.from(object).forEach((item) => {
    if (item.firstChild.checked == true) {
      item.style.display = "none";
    } else item.style.display = "block";
  });
}

function show_complete(e) {
  //traverse ul
  //if checked -show
  //else - dont show

  let obj = document.getElementById("mylist");

  let object = obj.children;

  Array.from(object).forEach((item) => {
    if (item.firstChild.checked == true) {
      item.style.display = "block";
    } else item.style.display = "none";
  });
}

function clear_complete(e) {
  let obj = document.getElementById("mylist");

  let object = obj.children;

  Array.from(object).forEach((item) => {
    if (item.firstChild.checked == true) {
      mylist.removeChild(item);
    } else item.style.display = "block";
  });
}

function update_counter() {
  let counter = document.getElementById("setme");

  counter.textContent = count + " items left";
}
